module qr

require (
	github.com/aws/aws-lambda-go v1.8.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/skip2/go-qrcode v0.0.0-20171229120447-cf5f9fa2f0d8
	github.com/stretchr/testify v1.2.2 // indirect
)
