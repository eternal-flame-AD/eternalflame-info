package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var AcceptableHosts = []string{
	"github.com",
}

func stringsContain(key string, set []string) bool {
	for _, s := range set {
		if s == key {
			return true
		}
	}
	return false
}

func intsContain(key int, set []int) bool {
	for _, i := range set {
		if i == key {
			return true
		}
	}
	return false
}

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	b64 := false
	if b64Raw, _ := request.QueryStringParameters["b64"]; b64Raw == "1" {
		b64 = true
	}
	method, ok := request.QueryStringParameters["method"]
	if !ok {
		method = "GET"
	}
	rawURL, ok := request.QueryStringParameters["url"]
	if !ok {
		return nil, errors.New("url not set")
	}
	expectedStatus := []int{}
	if rawStatus, ok := request.QueryStringParameters["status"]; ok {
		statusCodes := strings.Split(rawStatus, ",")
		for _, code := range statusCodes {
			if codeInt, err := strconv.Atoi(code); err != nil {
				return nil, err
			} else {
				expectedStatus = append(expectedStatus, codeInt)
			}
		}
	} else {
		expectedStatus = append(expectedStatus, 200)
	}
	if url, err := url.Parse(rawURL); err != nil {
		return nil, errors.New("url is not valid")
	} else if !stringsContain(url.Host, AcceptableHosts) {
		return nil, fmt.Errorf("host %s is not allowed", url.Host)
	}
	req, err := http.NewRequest(method, rawURL, nil)
	if err != nil {
		return nil, err
	}
	client := http.Client{
		Timeout: 10 * time.Second,
	}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("remote returned with %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if b64 {
		b64w := bytes.NewBuffer([]byte{})
		enc := base64.NewEncoder(base64.StdEncoding, b64w)
		if _, err := enc.Write(body); err != nil {
			return nil, err
		}
		return &events.APIGatewayProxyResponse{
			StatusCode:      res.StatusCode,
			Body:            b64w.String(),
			IsBase64Encoded: true,
		}, nil
	} else {
		return &events.APIGatewayProxyResponse{
			StatusCode: res.StatusCode,
			Body:       string(body),
		}, nil
	}
}

func main() {
	// Make the handler available for Remote Procedure Call by AWS Lambda
	lambda.Start(handler)
}
