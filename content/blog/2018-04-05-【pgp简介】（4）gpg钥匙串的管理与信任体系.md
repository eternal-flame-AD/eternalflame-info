---
title: 【PGP简介】（4）gpg钥匙串的管理与信任体系
author: Eternal-flame-AD
layout: post
identifier: 7757567a29071ac67024eea088eec76d
date: 2018-04-05T12:12:50+00:00
categories:
  - 技术
  - 日常

---
在我们实际使用PGP进行通讯的时候，首先就有一个非常核心的问题：我们究竟如何来管理自己所拥有的密钥呢？ PGP使用了一个钥匙串的体系来管理用户的密钥。下面我用gpg程序演示：

我们可以通过`gpg --list-keys`命令或者图形界面来查看钥匙串中的公钥，`gpg --list-secret-keys`来查看私钥：
  
下面我们来看一个pgp公钥记录（这是我的公钥~）

{{< highlight INI >}}
pub rsa4096 2018-01-19 [SCA] [expires: 2019-01-19]
356982FADCBDDC60DBAD25E84CBBC48C2F2FF36B
uid [ultimate] Eternal_flame-AD_extend (extended) &lt;eternal_flame-AD@protonmail.com&gt;
uid [ultimate] Eternal_flame-AD(extended) &lt;eternal_flame-AD@protonmail.com&gt;
sub rsa4096 2018-01-19 [E] [expires: 2019-01-19]
{{< / highlight >}}

我们看到，这个公钥有两个用户标识（uid），还有一个子密钥，2018-01-19生效，有效期到2019-01-19，使用的是rsa4096，上面可以看到我的名称和email地址，[ultimate]这个密钥因为被赋予了ultimate级别的信任所以有效性也是绝对可靠的（因为这是我自己的密钥= = **不要给予任何外来的密钥最高信任**）

关于信任级别的问题，可以使用`gpg edit-keys [uid]`进入密钥编辑后查看，我们来看这个密钥（因为是别人的所以我隐藏了指纹和用户标识）：

{{< highlight INI >}}
pub  rsa4096/XXXXXXXXXXXXXX
     created: 2018-02-26  expires: never       usage: C
     trust: never         validity: full
sub  rsa2048/XXXXXXXXXXXX
     created: 2018-02-26  expires: 2026-02-24  usage: E
sub  rsa2048/XXXXXXXXXXXXX
     created: 2018-02-26  expires: 2026-02-24  usage: SA
[  full  ] (1). XXXXXXX <XXXXXXXXX;
{{< / highlight >}}

可以看到trust（信任）一栏是never，validity（有效性）是full，这两个有什么区别呢？首先要从validity说起，validity指的是对一个密钥的有效性的说明，是由gpg程序计算出的，而非用户指派，一个密钥要被认为是有效的，就必须满足以下的条件：

  * 密钥在有效期内
  * 密钥没有被吊销（吊销属于密钥操作，之后会说到）
  * 密钥已经被另一个full级别或ultimate级别trust的密钥签名，或被多个（通常是3个）以上的marginal级别trust的密钥签名

上面的前两个应该比较简洁，第三个就要补充一个知识点了：我之前说到私钥可以给信息签名，这里的签名，不仅仅是对我发出的消息进行签名，还可以对其它的公钥进行签名，以此表明签名者认为该公钥是有效的。例如，你收到了来自朋友的一个公钥，你再将它导入钥匙串之后，gpg程序默认是不认为这个公钥有效的，只有你自己面对面或使用其他可靠方式确认公钥有效性以后，你使用自己的私钥（ultimate级别信任）签名之后，这个公钥才能在钥匙串中被标为有效。而对于full和marginal级别密钥签名的其他密钥，gpg分别有一套处理方法：这里就要说到密钥的trust了，gpg一共有一下五种trust级别，可以在编辑trust时看到：

<pre>Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
</pre>

当你试图更改你对一个公钥的trust时，gpg会提示上面这个消息。

  1. 我不知道这个密钥是否可信，这是新导入的公钥的默认信任度。
  2. 我不信任这个密钥，例如这是一个陌生人的公钥，你认为他不一定能够做到在对密钥签名前进行可靠的验证，因此他有可能有意或无意地给伪造的密钥签名，所以其它密钥不应当因为被他的密钥签名而得到有效性上的验证。可能有些绕，我再理一下，比如Alice收到了Bob的公钥，他们见面核对了签名，因此Alice用自己的私钥对Bob的公钥进行了签名，Bob的公钥因此就在Alice的钥匙串上被认为是有效的了，然而Alice认为Bob不是很会用pgp，有可能随便给别人的密钥签名，所以给予了none(2)的信任级别，果然有一天Bob在没有经过验证的情况下签名了Eve伪造的Charlie的密钥，并且将签名后的密钥传到了hkp服务器上（管理pgp密钥的公用服务器），但这个伪造的Charlie的密钥虽然经过Bob公钥的签名，仍然不会通过Alice钥匙串的有效性验证，因为gpg认为none(2)级别的密钥签名的其他密钥不一定有效。
  3. 边际信任。我不认为这个人签出的其他密钥100%有效，但如果一个密钥被三个或三个以上边际信任的密钥签名，那么有理由相信这个密钥是有效的。这是我比较经常使用的选项，一般用于较为熟悉的人。
  4. 完全信任。如果该密钥被认为有效，那么它签出的其他密钥都将被认为是有效的（和ultimate接近）。一般用于非常信任的人之间。
  5. 最高信任。不论该密钥当前计算结果是否是有效，它签出的其他密钥都被认为有效。一般只用于自己的密钥。