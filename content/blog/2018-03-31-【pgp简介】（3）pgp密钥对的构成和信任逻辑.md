---
title: 【PGP简介】（3）PGP密钥对的构成和信任逻辑
author: Eternal-flame-AD
layout: post
identifier: 738c90d60a61e0bd8b4b0f1a8e9f3544
date: 2018-03-31T04:00:31+00:00
categories:
  - 技术
  - 日常

---
对于一个PGP密钥对，除了公私钥内容之外，它还另外包括了一下信息（这些信息是被这个密钥签名过的，因此只有私钥的所有者能够更改它们）：

  1. 用户标识 
      * 用户标识，通常是姓名和电子邮件地址，是用来标识这个PGP密钥的所有者的（注意我这里用的是**密钥的所有者** 不是**用户标识上的姓名** 两者的区别我会在下面讲到）
  2. 有效期 
      * 有效期是密钥的有效时间范围，这个标识是为了保证密钥只在一段时间内有效（比如我的PGP密钥目前有效期是从18年1月19日到19年1月19日），每次使用这个密钥时，PGP程序会核对计算机上的时间和密钥的有效期，如果不在有效期内则会发出警告。设计有效期的目的主要是为了最大程度上减少因为私钥泄露等问题导致的后果，有效期短的密钥需要经常更新（不要忘了，私钥拥有者是可以更改有效期的），推荐每次更新他人的公钥的时候都要验证密钥的来源。
  3. 指纹 
      * 指纹是对整个密钥的一个hash，由密钥的其他部分经过散列计算而来，主要是用来： 
          * （1）标识这个密钥，在密钥服务器上查找公钥时，常常会使用指纹来搜索（例如你可以在hkp服务器上搜索我的公钥指纹_0x356982FADCBDDC60DBAD25E84CBBC48C2F2FF36B 来获得我完整的公钥_）
          * （2）验证密钥的完整性，比如，今天Alice和Bob见面，它们希望确定之前通过QQ等途径交换的公钥是否就是对方的公钥，他们不必逐字比对完整的公钥（那可能会花费大量时间），而只需要比对密钥指纹，基本就可以确认他们公钥的完整性。
  4. 子密钥 
      * 子密钥是隶属与这个主密钥的子密钥，可以理解为子密钥用于日常的加密/解密/签名/验证，而主密钥用来操作子密钥（增加子密钥/更改有效期/吊销/……）。一般只有命令行工具才提供比较自由的子密钥操作支持，所以为了通俗一点这里就不介绍具体用法了，引用一个[博文][1]

&nbsp;

但是，现在就出现了一个问题，我们假设Alice和Bob使用某种不安全聊天工具交换公钥的时候，Eve（服务器）在中间不仅可以监听，还可以截获/伪造消息，那么，Eve完全有能力自己生成两个密钥对，分别填上Alice和Bob的用户标识，并把假冒的公钥发给Alice和Bob，这样Alice和Bob都会发现自己拿到了用户标识写着对方的公钥（实际上是Eve的公钥，对应的私钥在Eve手中），而当他们再进行通信的时候，他们加密出的所有内容都可以被Eve解密了（例如：Alice把她的消息用自己的私钥签名，之后通过**他认为的**Bob的公钥加密发出去；Eve截获这个消息，用自己手中的私钥解密（因为Alice手中认为的Bob的公钥其实是Eve生成的 这时Eve就获得**消息明文**了） 再用自己假冒的Alice的私钥签名 用Bob真正的公钥加密发给Bob；Bob就得到了一条看起来由Alice签名并加密给自己的信息） 这种情况就被称为[中间人攻击（MITM）][2]，为了确定密钥交换过程中没有中间人的干扰，我们就需要去验证双方收到的公钥是否和发出的相匹配，这一点PGP并不能帮你去做，你只能通过某种安全的渠道（例如见面），逐字核对密钥指纹来确定Alice获得的公钥就是和Bob手中的私钥相对应。在下一篇中我会详细说到PGP是如何处理对公钥的信任的问题的。

 [1]: https://www.cnblogs.com/andypeker/p/6124045.html
 [2]: https://en.wikipedia.org/wiki/Man-in-the-middle_attack