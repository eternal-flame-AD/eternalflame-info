---
title: Horns Up, Hearts Open
date: 2019-07-01T11:29:04+08:00
author: eternal-flame-AD
layout: post
identifier: 375ca559b8fc2237021fc4074bcf171f
tags: 
  - UT
categories:
  - 随想
signature: null
---

刚刚结束的 UT Orientation 让小透明觉得受益匪浅，于是小透明便在回程的航班中写下这段文字，记录自己第一次设身处地地接触美国文化留下的体验。

## The Texan Way

在到达的开头几天，小透明参观了 Bullock Texas State History Museum。博物馆设计的非常精致，从一楼开始，整场展览就如同用展品讲故事一般，把德克萨斯州的历史完整地讲述了一遍。小透明在展览中看到了德州人们为自己斗争的历史，了解了 The Lone Star State 的称呼来源，也对 Civil War 等美国历史上的重大事件有了一些站在一个州的角度上看的独特理解。

## The Caring Way

小透明很喜欢美国餐厅的服务。

小透明一直不喜欢去中餐馆。一是因为自己不会主动去，去的原因一般都是要见亲戚，于是就条件反射性地难受，二是因为小透明觉得中国餐馆的服务让自己不是很能接受。

就小透明观察而言，中国餐厅服务员的服务模式大概分为两种：

- 问啥答啥，客人干啥我不 care，我只负责抄菜名上菜。
- 客人就是上帝，我要想方设法把自己放低让客人感到自己在这里是优越的，是被“服务”的。

这是让人喜欢的服务吗？至少小透明不喜欢，与其这样小透明觉得不如在 web 上对着移动设备点菜 —— 讽刺的是不少中国餐馆已经开始这么做了哈哈哈，小透明挺喜欢，至少相比服务员来说不那么尴尬。

小透明第一次决定走进一个真正的餐馆是在 The Domain， Austin 最大的 Mall。那是一家日式餐饮店，小透明自己是实力眼瞎，一开始没有看到饮料单上的 Cocktail，尝试点了一杯，被服务员要求出示 ID。小透明一开始还很懵，因为身上东西比较多所以随手能掏到的只有 Student ID，后来服务员才解释因为这是酒精饮料所以需要核实小透明的年龄，小透明这才发现自己的实力眼瞎本质。不过这一段小插曲也开始了小透明和服务员一段日常对话——小透明知道 UT 是一个很大的 community，但小透明没想到会在餐馆里就遇到 UT Student。我们聊了他在 UT 的生活体验，小透明选择 UT 的原因…… 小透明真实感觉到，这才是服务员的艺术：作为服务员，最重要的能力不是背下菜单，也不是一手拿三个盘子的绝技，而是在简短几分钟的交流中让客人感到自己是实实在在地被一个独特的人服务的艺术。

有人说餐厅取消小费制度是化繁为简，是进步，但当服务员递上 Receipt，需要小透明自己填入小费金额的时候，小透明第一次实实在在体验到小费制度实实在在造成了这样一种轻松健康的服务氛围——谁会愿意给一个可以被一个 Web 界面替代的机器人般的服务员小费呢？小透明每走进一个不同的餐馆，希望看到的都是一个不同的、独一无二的服务员，小透明认为这就是自己付小费所希望得到的——而且小透明的确得到了。

不论是奢饰品商店，还是路边的小便利店，每当小透明结账离开的时候，收银员总会说一句 "Have a good day“ 作为告别。是啊，希望自己有快乐的一天。小透明在中国听到的只是”你应该快乐“啊，大家都快乐你为什么不快乐。当小透明告诉别人自己不开心的时候，通常得到的只是一个完全不感觉好笑甚至还觉得 offensive 的玩笑。

## The Realistic Way

小透明和同学开玩笑，说 UT Orientation 是”资本享乐主义“的新生报到活动——除了 College Meeting 和 Class Resistration 等，活动不是看表演就是 Party 和其他狂欢活动。

但是不得不说，这次小透明真的学到了很多很多。

没有没用的政治讨论和并不能打动人心的爱国主义教育，也没有追寻所谓梦想的空话，精心准备的一个一个 show 和 presentaion 让小透明真真切切感受到了如何做一个独一无二的，出色的 college student。整场 Orientation，从来没有任何一个活动提到出色的 student 是一个 perfect GPA，或是一项宏伟的研究计划，或是 Student Government 的领导人。这篇文章的标题，_Horns Up, Hearts Open_，是小透明在这次活动中印象最深的活动的主题。Everyone has their own unique (combination) of social identities, and the best part is you being you. 每个人都有自己的特点，难道不正是生活的有趣之处嘛？如果每个人都向着一个所谓的“完美”的目标迈进，永远有人会在小透明之前，世界不会因为失去小透明而流下一滴泪水——无非是大江中的一滴水罢了，想必做一滴水也不会在乎自己会不会蒸发的无影无踪的吧。

## The Loving Way

在 Orientation 刚开始的 Campus Tour 上，小透明是非常紧张的：UT 是一个 90% 以上学生来自德克萨斯州的学校，六七月的 Orientaion Session 里 In-State Student 的比例更是大。小透明甚至还没离开等待室就发现自己的同学们自发地组成了两三人的小队，而自己却被排除在外了。

虽然这样的事情小透明在出发之前已经意料到了，但当自己在和团队一起在校园里行进的时候，只有自己身边没有朋友的情形还是十分让小透明苦恼的。小透明虽然内心十分希望有同学能够和自己开始一段对话，但还是不能客服心中不希望尴尬的念头，装作一副十分 Chill 的样子，让大家认为小透明是自己希望一个人的。

交友上的第一个突破是在 Longhorn State of Mind 集会中取得的。我和坐在旁边的 S 同学突然对上了眼神，相视一笑之后我们开始了一段小小的对话，我们是同一个专业，我们在之后的 Orientation 活动中也经常一起参加，相信开学后还能经常见到她。

小透明发现自己的 Wing 中有一位同学始终是孤零零一个人，当别人主动接近他的时候，他便从嘴角挤出一丝笑容，而几句话过后又回到了一个人的状态。

在 Orientation 最后一天，小透明在 Gender and Sexuality Center Open House Session 上遇到了他。（其实小透明在进入之前就预料到了他可能会在那里，小透明的雷达终于准了一次hhh）

虽然一个小时很快就过去了，但是小透明真的感觉自己找到了归属感。I am not alone, and I am proud of being myself. 小透明也和那位同学交了朋友，我们在同一个 FIG 中，也就是说我们开学后每天都会见面啦~

## My Own Way

在 College Meeting 上，教授和我们做了一个 T-shirt Giveaway 的活动——赠送特别的 CNS T-shirt 给来到这次 Orientation 的同学中的特别的几位——年龄最小/大的，能背诵 PI 的值最多位的，能记住 UT 大多数 Football Match 比分的。

教授拿起了一件灰色的印着 _Texas Science_ 的 T-shirt。

> Now this shirt will go to, the one who lives farest away from Austin.

台下开始了竞拍一般的呼喊声： Tennessee, New York, Minnesota。

小透明没有预料到在 CNS 这样一个庞大的学院中，大概有两三百人的会场中，现在该是自己发声的时候了。

小透明知道在这里自己是特别的。
 
但特别不代表小透明需要磨平自己所有的棱角，把自己埋在人海中。

不，小透明不仅在这里是特别的，小透明永远是特别的。小透明不是“一个中国人”，也不是“一位大学生”，小透明就是小透明。
 
小透明举起了自己的手。

China. 小透明大声地说出。
 
小透明第一次为自己的独特身份感到由衷地自豪。

全场同学的眼神都集中到了小透明身上。

> Congratuations then, looks like no one is going to beat you today.

小透明从眼神中能读取到，大家是接受自己的。

One is born to be different, and the only way to stand out is to tell others that you are different. 这是小透明在这次 College Meeting 上获得的最大收获。

Loneliness fighting back again seems to be like it never ends. Give us hope through the love of peaceful shine on me.

祝一切顺利。