---
title: "About This Site"
date: 2018-12-27T17:49:36+08:00
identifier: a5c9f7900d7b96eb0d7a36c5ef1a73be
description: null
---

## Facts

This website is hosted on [Netlify](https://www.netlify.com/).

The back-end language for this website is [Go](https://www.golang.org/).

The source code for this website is available [here](https://gitlab.com/eternal-flame-AD/eternalflame-info).

## Open Source Attributions

| Name | Type | License |
| --- | --- | --- |
| [Hugo](https://github.com/gohugoio/hugo) | Generator - SSG | [Apache 2.0](https://github.com/gohugoio/hugo/blob/master/LICENSE) |
| [fontTools](https://github.com/fonttools/fonttools) | Generator - Font Processing | [MIT](https://github.com/fonttools/fonttools/blob/master/LICENSE) |
| [github-calendar](https://github.com/IonicaBizau/github-calendar/) | Integration - GitHub | [MIT](https://github.com/IonicaBizau/github-calendar/blob/gh-pages/LICENSE) |
| [Bootstrap](https://github.com/twbs/bootstrap) | Frontend - CSS | [MIT](https://github.com/twbs/bootstrap/blob/v4-dev/LICENSE) |
| [Highlight.js](https://highlightjs.org/) | Frontend - JS | [BSD](https://github.com/highlightjs/highlight.js/blob/master/LICENSE) |
| [jQuery](https://jquery.com/) | Frontend - JS | [MIT](https://github.com/jquery/jquery/blob/master/LICENSE.txt) | 
| [Lodash](https://lodash.com/) | Frontend - JS | [MIT](https://github.com/lodash/lodash/blob/master/LICENSE) | 
| [Backbone.js](https://github.com/jashkenas/backbone) | Frontend - JS | [MIT](https://github.com/jashkenas/backbone/blob/master/LICENSE) |
| [Font Awesome](https://fontawesome.com/) | Assets - Icons | [Other](https://fontawesome.com/license/free) |
