package main

import "fmt"

func toUnicodeCodePoint(r rune) string {
	return fmt.Sprintf("%U", r)
}
