module font

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/gammazero/deque v0.0.0-20180920172122-f6adf94963e4 // indirect
	github.com/gammazero/workerpool v0.0.0-20180920155329-48371c973101
	github.com/tdewolff/minify/v2 v2.3.8
)
