package main

import (
	"crypto/md5"
	"encoding/hex"
)

func SignStringMD5(s string) string {
	digest := md5.New()
	digest.Write([]byte(s))
	hash := digest.Sum(nil)
	return hex.EncodeToString(hash)
}
